#!/bin/bash
cd /home/test
if [[ $1 == "goodix" ]]; then 
	python3 build.py -p gr5515_sk_iotlink_demo@goodix -f
elif [[ $1 == "bestechnic" ]]; then
	python3 build.py -p display_demo@bestechnic -f
elif [[ $1 == "bearpi" ]]; then
	python3 build.py -p bearpi_hm_nano@bearpi -f
else
	./test/xts/tools/lite/build.sh product=wifiiot xts=acts
fi
