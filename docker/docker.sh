#!/bin/bash
cd $DEP_BUNDLE_BASE
docker version &> /dev/null
if [ $? -ne 0 ];
then
	sudo apt-get remove docker docker-engine docker.io
    sudo apt-get install apt-transport-https ca-certificates curl gnupg2 software-properties-common -y
    curl -fsSL http://mirrors.huaweicloud.com/docker-ce/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] http://mirrors.huaweicloud.com/docker-ce/linux/ubuntu $(lsb_release -cs) stable"
    sudo apt-get update
    sudo apt-get install docker-ce -y
fi
docker pull $imageId
DOCKER_DIR=/home/test
docker run --net=host -it -v $DEP_BUNDLE_BASE:$DOCKER_DIR $imageId /bin/bash $DOCKER_DIR/ohos_bundles/@ohos/mini_system/scripts/dist.sh $1
