#!/bin/bash
cd $DEP_BUNDLE_BASE

# link build.py
if [ ! -f build.py ]; then
	ln -s build/lite/build.py build.py
fi

#link .gn
if [ ! -f .gn ]; then
	ln -s build/core/gn/dotfile.gn .gn
fi

# link kv_store
if [ -d utils/native/lite ] && [ ! -d utils/native/lite/kv_store ] && [ -d foundation/distributeddatamgr/appdatamgr/frameworks/innerkitsimpl/kv_store ];
then
	cd utils/native/lite
	ln -s ../../../foundation/distributeddatamgr/appdatamgr/frameworks/innerkitsimpl/kv_store kv_store
	cd -
fi

# link build.sh
if [ ! -f build.sh ]; then
	ln -s build/build_scripts/build.sh build.sh
fi

