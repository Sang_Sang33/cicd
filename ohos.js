const fs = require("fs")
const path = require("path")
const bundlePath = path.relative(__dirname, "bundle")
const dirs = fs.readdirSync(bundlePath, { encoding: "utf-8" })
dirs.forEach(dir => {
    try {
        const jsonDir = path.resolve(bundlePath, dir, "bundle.json")
        if (fs.existsSync(jsonDir)) {
            const jsonData = JSON.parse(fs.readFileSync(jsonDir).toString())
            jsonData.version = "3.1.1"
            if (jsonData.segment && jsonData.segment.destPath && jsonData.segment.destPath.startsWith("applications/")) {
                jsonData.component.subsystem = "applications"
            }
            fs.writeFileSync(jsonDir, JSON.stringify(jsonData, null, 4))
        }
    } catch (error) {
        console.log(`failed: ${dir}`)
    }
})