### ohos_standard_system

## 简介
### 标准系统设备(standard system)
面向应用处理器的设备，支持的设备最小内存为128MiB, 可以提供增强的交互能力、GPU以及硬件合成能力、更多控件以及动效更丰富的图形能力、完整的应用框架。可支撑的产品如带屏IOT设备、轻智能手机。

## 如何使用
本发行版预置了rk3568一种形态的所有组件。你可以根据自己的需求挑选及配置适合自己的组件。

⚠️请在Linux环境下执行

1. 进入一个项目目录，在工作目录下执行如下命令

```shell
hpm init -t dist
hpm i @ohos/standard_system
```

说明：

```
hpm init -t dist ： 表示通过dist模板创建一个项目
hpm i xx 表示安装一个xx组件/发行版到你的项目，此过程会将所有依赖的组件（包括间接依赖）和工具下载安装到本机
```

2. 执行编译命令

```shell
hpm dist 
```
执行`rk3568`发行版的编译构建


说明：

```
hpm dist 表示执行发行版的编译构建，将开始制作镜像，运行结束后，文件将会输出到out目录下。
```

docker pull swr.cn-north-4.myhuaweicloud.com/sang/swr-demo-2048:latest