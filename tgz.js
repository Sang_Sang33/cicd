const fs = require("fs")
const path = require("path")
const bundlePath = path.relative(__dirname, "bundle")
const dirs = fs.readdirSync(bundlePath, { encoding: "utf-8" })
dirs.forEach(dir => {
    try {
        const jsonDir = path.resolve(bundlePath, dir, "bundle.json")
        if (fs.existsSync(jsonDir)) {
            const jsonData = JSON.parse(fs.readFileSync(jsonDir).toString())
            const name = jsonData.name.split("/")[1]
            const tgzName = path.resolve(bundlePath, dir, `@ohos-${name}-3.1.0.tgz`)
            if (!fs.existsSync(tgzName)) {
                console.log(`failed: ${dir}`)
            }
        } else {
            console.log(`failed: ${dir}`)
        }
    } catch (error) {
        console.log(`failed: ${dir}`)
    }
})