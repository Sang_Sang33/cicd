const fs = require("fs")
const path = require("path")
const os = require("os")
const repoString = fs.readFileSync(path.resolve(__dirname, "list.txt")).toString()
const repos = [...new Set(repoString.split(os.EOL))]
fs.writeFileSync(path.resolve(__dirname, "list.txt"), repos.join(os.EOL))
