### ohos_small_system

## 简介
### 小型系统设备(small system)
面向应用处理器例如Arm Cortex-A的设备，支持的设备最小内存为1Mib，可以提供更高的安全能力、标准的图形框架、视频编解码的多媒体能力。可支撑的产品如智能家居领域的IP Camera、电子猫眼、路由器以及智慧出行域的行车记录仪等。

## 如何使用
本发行版预置了hispark_taurus、hispark_taurus_linux两种形态的所有组件。你可以根据自己的需求挑选及配置适合自己的组件。

⚠️请在Linux环境下执行

1. 进入一个项目目录，在工作目录下执行如下命令

```shell
hpm init -t dist
hpm i @ohos/small_system
```

说明：

```
hpm init -t dist ： 表示通过dist模板创建一个项目
hpm i xx 表示安装一个xx组件/发行版到你的项目，此过程会将所有依赖的组件（包括间接依赖）和工具下载安装到本机
```

2. 执行编译命令

```shell
hpm dist 
```
执行`hispark_taurus_linux`发行版的编译构建

```shell
hpm dist liteos
```
执行`hispark_taurus`发行版的编译构建


说明：

```
hpm dist 表示执行发行版的编译构建，将开始制作镜像，运行结束后，文件将会输出到out目录下。
```