const path = require('path');
const os = require('os')
// 存在源码的目录，流水线参数传入
const REPO_DIR = path.resolve(__dirname, "..", "..", "..", "..", "repo")
// const TEMPLATE_DIR = path.resolve(__dirname, "..", "..", "..", "..", "distrubtion")
const TEMPLATE_DIR = path.resolve(__dirname, "..", "..", "template")

const BUNDLE_DIR = path.resolve(__dirname, "..", "..", "..", "..", "bundle")
const CSV_PATH = path.resolve(__dirname, "..", "..", "..", "..", "matrix_product.csv")
const DEFAULT_XML = path.resolve(__dirname, "..", "..", "..", "..", "default.xml")
const DEFAULT_CONFIG_DIR = path.resolve(os.homedir(), '.hpm')
const KEY_FOLDER = path.resolve(__dirname, "..", "key")

// 可能的readme文件名
const DEAFULT_README_NAMES = [
  "README", 
  "README.rst", 
  "README.txt", 
  "README_zh.md", 
  "TaC-mksh.txt", 
  "README_zh.md", 
  "readme.md", 
  "Readme.markdown",
  "readme.txt"
] 

//  无法解析出来，只能预先默认定义的readnme路径
const README_PATH_MAP = {
  "third_party_flutter": { en: "flutter/README.md" },
  "third_party_FatFs": { en: "source/00readme.txt" }
}

//  无法解析出来，只能预先默认定义的license路径
const LICENSE_PATH_MAP = {
  "third_party_flutter": "flutter/LICENSE",
  "device_hisilicon_hispark_pegasus": "NOTICE",
  "device_hisilicon_hispark_taurus": "NOTICE",
  "third_party_ffmpeg": "COPYING.LGPLv3",
  "kernel_linux_4.19": "LICENSES/preferred/GPL-2.0",
  "kernel_linux_5.10": "LICENSES/preferred/GPL-2.0",
  "third_party_pcre2": "pcre2/LICENCE"
}

// 找不到readme文件的仓名
const NONE_README = ["third_party_mtd_utils", "third_party_cmsis", "third_party_libuuid"]

// 找不到LICENSE文件的仓名
const NONE_LICENSE = ["third_party_qrcodegen", "device_board_hisilicon", "device_soc_hisilicon"]

// 存在一些仓里暂无bundle.json文件，预备的json文件目录
const BUNDLE_REPO = path.resolve(__dirname, "..", "bundleRepo")

// 从csv文件中解析发行版依然缺失的组件 
const INCLUDE_REPOS_FOR_PRODUCT = {
  "hispark_pegasus": [
    "prebuilts_lite_sysroot", 
    "third_party_cmsis"
  ],
  "hispark_taurus": [
    "applications_camera_screensaver_app", 
    "update_ota_lite", 
    "third_party_icu", 
    "third_party_libcoap", 
    "third_party_mksh", 
    "third_party_toybox"
  ],
  "hispark_taurus_linux": [
    "update_ota_lite",
    "third_party_cryptsetup",
    "global_cust_lite",
    "build",
    "applications_camera_screensaver_app",
    "third_party_mksh",
    "powermgr_battery_lite",
    "third_party_toybox",
    "third_party_LVM2",
    "third_party_JSON-C",
    "third_party_libuuid",
    "third_party_popt",
    "third_party_icu",
    "third_party_libcoap"
  ]
}

// 从csv文件中解析发行版依然多余的组件 
const EXCLUDE_REPOS_FOR_PRODUCT = {
  "hispark_pegasus": [
    "manifest",
    "communication_ipc_lite",
    'security_permission',
    'xts_hats',
    "powermgr_battery_lite",
    'third_party_FatFs',
    'third_party_FreeBSD',
    'third_party_NuttX',
    'third_party_bounds_checking_function',
    'third_party_gn',
    'third_party_lwip',
    'third_party_mtd_utils',
    'third_party_musl',
    'third_party_openssl',
    'third_party_unity',
    'third_party_zlib',
  ],
  "hispark_taurus": [
    "manifest", 
    "device_hisilicon_third_party_ffmpeg"
  ],
  "hispark_taurus_linux": [
    "manifest", 
    "device_hisilicon_third_party_ffmpeg",
    "kernel_linux",
    'ai_engine',
    'communication_softbus_lite',
    'hiviewdfx_blackbox',
    'hiviewdfx_hidumper_lite',
    'hiviewdfx_hitrace',
    'hiviewdfx_hiview_lite',
    'iothardware_peripheral',
    'xts_hats'
  ],
  "bes2600": [
    "manifest"
  ]
}

module.exports = {
  REPO_DIR,
  BUNDLE_DIR,
  CSV_PATH,
  DEFAULT_XML,
  DEFAULT_CONFIG_DIR,
  KEY_FOLDER,
  TEMPLATE_DIR,
  DEAFULT_README_NAMES,
  README_PATH_MAP,
  LICENSE_PATH_MAP,
  NONE_README,
  BUNDLE_REPO,
  NONE_LICENSE,
  INCLUDE_REPOS_FOR_PRODUCT,
  EXCLUDE_REPOS_FOR_PRODUCT
};
