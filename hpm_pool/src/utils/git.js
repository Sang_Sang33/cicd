const { default: simpleGit } = require("simple-git")
const path = require("path")
class Git {
    constructor(repoName, localPath) {
        this.repoName = repoName
        this.localPath = localPath
    }
    get repoPath() {
        return `https://gitee.com/openharmony/${this.repoName}.git`
    }
    clone(codeBranch) {
        return simpleGit().clone(this.repoPath, path.resolve(this.localPath, this.repoName), [ "-b", codeBranch ])
    }
}
module.exports = Git