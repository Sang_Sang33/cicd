#!/bin/bash
cd $DEP_BUNDLE_BASE
if [ ! -f build.py ]; then
	ln -s build/lite/build.py build.py
fi
cd -

hpm run config_hb

if [[ $debug ]]
then
    python3 ../../../build.py -p ipcamera_hispark_taurus@hisilicon -f -c clang
else
    python3 ../../../build.py -p ipcamera_hispark_taurus@hisilicon -f -c clang -b release
fi
