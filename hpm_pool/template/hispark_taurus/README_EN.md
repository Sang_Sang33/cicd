# hispark_taurus

Full-stack lite design for the kernel, UI, media, and JavaScript development framework; a wide range of UI components, a comprehensive graphics stack, a broad set of multimedia and distributed scheduling features.

## **Usage**

This distribution contains all the bundles required for Small-System Device development, and you can inherit directly from the distribution **@ohos/hispark_taurus**  without configuring the components from blank.

⚠️Please run the following commands in Linux environment

1. Open a project directory and run the following command in the working directory:

```shell
hpm init -t dist
hpm i @ohos/hispark_taurus
```

remark：

```
hpm init -t dist ： create a project using the 'dist' template
hpm i xx : install a xx bundle/distribution to your project. This process will download and install all depenent bundles(including indirect dependencies) and tools to the local environment.
```

2. Run the distribution command.

```shell
hpm dist
```

remark:

```
hpm dist :Starts to compile and build the distribution. After the operation is complete, the image files are outputed to the 'out' directory。
```

## **Introduction to hispark_taurus**

The system on chip (SoC) used by the **hispark_taurus** development board is Hi3516D V300. Hi3516D V300 is a next-generation SoC designed for the industry-dedicated smart HD IP camera. It introduces a next-generation image signal processor (ISP), the latest H.265 video compression encoder, and a high-performance NNIE engine, leading the industry in terms of low bit rate, high image quality, intelligent processing and analysis, and low power consumption.

## **Functions and Features**

| Component                | Description                                                  |
| ------------------------ | ------------------------------------------------------------ |
| Design for X (DFX)       | Provides the DFX capabilities, such as logging and printing. |
| Distributed Scheduler    | Sets up a distributed service platform in HarmonyOS by using a proxy between the primary and secondary devices. With the Distributed Manager Service, the primary device can start a Feature Ability (FA) deployed on the secondary device (a memory-constrained HarmonyOS device such as an IP camera or a lite wearable). |
| Graphics                 | Provides basic UI and container components, including buttons, images, labels, lists, animators, scroll views, swipe views, fonts, clocks, charts, canvases, sliders, and layouts, as well as the display output capability. |
| Multimedia               | Provides unified interfaces for you to develop media applications. With this subsystem, you can easily obtain media resources and stay focused on service development. |
| Security                 | Provides capabilities of application signature verification, encryption and decryption, security binding of devices, permission management, and security services. |
| Startup                  | Provides key service capabilities during system startup.     |
| JS Application Framework | Allows you to develop web-like applications across platforms. The framework uses Toolkit to pack your **.html**, **.css**, and **.js** files into a JavaScript bundle, parses the bundle, and renders it with view components of the C++ UIKit. |
| Kernel                   | Provides capabilities of process, scheduling, and memory management. |
| Driver                   | Provides the driver framework capabilities, including driver loading, driver service management, and the driver message mechanism. |

## **Reference**

- [Introduction to the Hi3516 Development Board](https://device.harmonyos.com/en/docs/start/introduce/oem_camera_start_3516-0000001052670587)
