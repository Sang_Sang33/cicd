#!/bin/bash
cd $DEP_BUNDLE_BASE
if [ ! -f build.py ]; then
	ln -s build/lite/build.py build.py
fi
cd -

hpm run config_hb

if [[ $debug ]]
then
    hb build -f -p wifiiot_hispark_pegasus@hisilicon
else
    hb build -f -p wifiiot_hispark_pegasus@hisilicon -b release
fi
