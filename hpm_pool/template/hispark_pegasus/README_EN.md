# hispark_pegasus

Full-stack lite design for the kernel, UI, media, and JavaScript development framework; a wide range of UI components, a comprehensive graphics stack, a broad set of multimedia and distributed scheduling features.

## **Usage**

This distribution contains all the bundles required for Mini-System Device development, and you can inherit directly from the distribution **@ohos/hispark_pegasus**  without configuring the components from blank.

⚠️Please run the following commands in Linux environment

1. Open a project directory and run the following command in the working directory:

```shell
hpm init -t dist
hpm i @ohos/hispark_pegasus
```

remark：

```
hpm init -t dist ： create a project using the 'dist' template
hpm i xx : install a xx bundle/distribution to your project. This process will download and install all depenent bundles(including indirect dependencies) and tools to the local environment.
```

2. Run the distribution command.

```shell
hpm dist
```

remark:

```
hpm dist :Starts to compile and build the distribution. After the operation is complete, the image files are outputed to the 'out' directory。
```

## **Introduction to hispark_pegasus**

The SoC used by the **hispark_pegasus** development board is Hi3861. The Hi3861 WLAN module is a 2 x 5 cm development board. It is a 2.4 GHz WLAN SoC chip that highly integrates the IEEE 802.11b/g/n baseband and radio frequency (RF) circuit. This board provides open and easy-to-use development and debugging environments and third-party components.

## **Functions and Features**

| Component              | Description                                                  |
| ---------------------- | ------------------------------------------------------------ |
| WLAN                   | Provides WLAN service, such as connecting to or disconnecting from a station or hotspot, and querying the state of a station or hotspot. |
| IOT Controller         | Provides the capability of operating peripherals, including the I2C, I2S, ADC, UART, SPI, SDIO, GPIO, PWM and flash memory. |
| Intelligent Soft Bus   | Provides the capabilities of device discovery and data transmission in the distributed network. |
| Hichainsdk             | Provides the capability of securely transferring data between devices when they are interconnected. |
| Huks                   | Provides capabilities of key management, encryption, and decryption. |
| System Service Manager | Provides a unified HarmonyOS service development framework based on the service-oriented architecture. |
| Bootstrap              | Provides the entry identifier for starting a system service. When the system service management is started, the function identified by **bootstrap** is called to start a system service. |
| Syspara                | Provides capabilities of obtaining and setting system attributes. |
| Utils                  | Provides basic and public capabilities, such as file operations and key-value (KV) storage management. |
| DFX                    | Provides the DFX capabilities, such as logging and printing. |
| XTS                    | Provides a set of HarmonyOS certification test suites.       |

## **Reference**

- [Introduction to the Hi3861 Development Board](https://device.harmonyos.com/en/docs/start/introduce/oem_wifi_start_des-0000001050168548)
