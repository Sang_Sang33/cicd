#!/bin/bash
cd $DEP_BUNDLE_BASE
if [ ! -f build.py ]; then
	ln -s build/lite/build.py build.py
fi

if [[ $debug ]]
then
    python3 build.py -p ipcamera_hispark_taurus_linux@hisilicon -f
else
    python3 build.py -p ipcamera_hispark_taurus_linux@hisilicon -f -b release
fi
cd -
