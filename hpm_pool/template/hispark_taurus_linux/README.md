# hispark_taurus_linux

全栈轻量化设计，包括内核、UI、媒体、JS 开发框架等，支持丰富的 UI 控件，完备的图形栈和多媒体能力、分布式调度能力。


## 如何使用

本发行版包含了小型系统设备开发需要的所有组件，你可以直接继承自该发行版，无需从头配置组件。

⚠️请在Linux环境下执行

1. 进入一个项目目录，在工作目录下执行如下命令

```shell
hpm init -t dist
hpm i @ohos/hispark_taurus_linux
```

说明：

```
hpm init -t dist ： 表示通过dist模板创建一个项目
hpm i xx 表示安装一个xx组件/发行版到你的项目，此过程会将所有依赖的组件（包括间接依赖）和工具下载安装到本机
```

2. 执行发行命令

```shell
hpm dist
```

说明：

```
hpm dist 表示执行发行版的编译构建，将开始制作镜像，运行结束后，文件将会输出到out目录下。
```

## 开发板介绍

hispark_taurus开发板采用Hi3516DV300。Hi3516DV300作为新一代行业专用Smart HD IP摄像机SOC，集成新一代ISP、业界最新的H.265视频压缩编码器，同时集成高性能NNIE引擎，使得Hi3516DV300在低码率、高画质、智能处理和分析、低功耗等方面引领行业水平。

## 功能特性

| 组件名                                      | 能力介绍                                                     |
| ------------------------------------------- | ------------------------------------------------------------ |
| DFX   (DFX)                                 | 提供DFX能力。包括：流水日志、时间打点等。                    |
| 分布式调度子系统（Distributed_scheduler）   | 通过主从设备服务代理机制，在操作系统上建立起分布式服务平台，支持主设备启动从设备(IP Camera、运动手表等小内存设备)FA的能力。 |
| 图形图像子系统（Graphic）                   | 提供基础UI组件和容器类组件，包括button、image、label、list、animator、scroll view、swipe view、font、clock、chart、canvas、slider、layout能力。提供显示屏输出能力。 |
| 多媒体子系统   （MultiMedia）               | 提供相机、音频、视频、音视频编码能力。                       |
| 安全子系统   （Security）                   | 提供应用验签、加解密、设备安全绑定、权限管理、安全态服务等能力。 |
| 启动恢复子系统（StartUp）                   | 提供子系统启动时，加载关键服务能力                           |
| JS开发框架子系统 (JS Application Framework) | 提供一套跨平台的类web应用开发框架，通过Toolkit将开发者编写的HTML、CSS和JS 文件编译打包成JS Bundle，然后再将JS Bundle解析运行成C++ UIKit的View 组件进行渲染。 |
| 内核子系统   (Kernel)                       | 提供进程、调度、内存管理等能力                               |
| 驱动子系统   (Driver)                       | 提供驱动框架，包括驱动加载、驱动服务管理和驱动消息机制。     |

## 参考资料

- [Hi3516快速入门](https://device.harmonyos.com/cn/docs/start/introduce/oem_camera_start_3516-0000001052670587)
