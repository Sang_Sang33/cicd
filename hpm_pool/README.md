## hpm_pool
运行在流水线上的脚本代码，用于发布OpenHarmony组件包到HPM官网上。

### 运行环境目录结构
最外层为manifest仓库的代码，cicd代码仓在manifest仓里

manifest                                    manifest代码仓
│  .gitignore
│  default.xml                              必须文件
│  devboard.xml
│  LICENSE
│  matrix_product.csv
│  other.txt
│  package-lock.json
│  package.json
│  README.md
│      
├─bundle                                    必须目录（自行创建），存在组件压缩包
│
├─repo                                      必须目录（自行创建），存放git clone下来的源码文件
│
├─cicd                                      cicd代码仓
│  │  .gitignore
│  │  README.en.md
│  │  README.md
│  │  
│  └─hpm_pool                               脚本代码相关目录
│      │  package-lock.json
│      │  package.json
│      │  README.md
│      │  
│      └─src
│          ├─constant
│          │      index.js                  常量文件
│          │      
│          ├─core
│          │      bundle.js                 
│          │      product.js           npm run product入口文件
│          │      key.js            
│          │      login.js                  登陆相关
│          │      packBundle.js             npm run bundle:pack入口文件
│          │      publishBundle.js          npm run bundle:publish入口文件
│          │      rc-parser.js
│          │      
│          ├─key                            存放公私密钥的目录
│          │      privateKey_8EBENJ2.pem
│          │      privateKey_Z7WHVTW.pem
│          │      publicKey_8EBENJ2.pem
│          │      publicKey_Z7WHVTW.pem
│          │      
│          ├─thread
│          │      index.js
│          │      
│          └─utils
│                  crypto.js                密钥加解密，签名相关代码
│                  csv.js                   解析csv文件代码
│                  git.js                   git clone代码
│                  http.js                  axios封装请求
│                  index.js                 
│                  url.js                   
│                  xml.js                   解析default.xml
│

### hpm_pool目录下主要命令介绍

* npm run product ${codeBranch}
    - 解析OpenHarmony manifest仓的default.xml文件，遍历clone所有的代码仓。
    - **codeBranch**: 从流水线传入的gitee代码仓分支参数，默认为**OpenHarmony-3.0-LTS**分支
    - 下载的代码仓会存入到repo目录下

* npm run key
    - 用于测试是否可以通过公私密钥登陆HPM网站，主要用于发布。
    - 登陆成功，需要将公钥上传到HPM网站，并将公私密钥文件存入到key目录下，以及用于加密私钥的本机mac地址写入到**getMacAddrs**函数中，否则私钥会解密失败。

* npm run bundle:pack ${version} ${loginUser}
    - 遍历repo目录下的代码仓，执行hpm pack命令，将tgz文件cp到bundle目录下
    - **version**: 指定bundle.json的版本号，默认**3.0.0-snapshot**
    - **loginUser**: 指定邀请码，用于publish时的登陆

* npm run bundle:publish ${version} ${loginUser}
    - 遍历repo目录下的代码仓，执行hpm publish命令
    - **version**: 指定bundle.json的版本号，默认**3.0.0-snapshot**
    - **loginUser**: 指定邀请码，用于publish时的登陆

