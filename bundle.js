const fs = require('fs');
const path = require('path');

const dependencies_json = require("./dependencies.json")
const error_dependencies_json = require("./error_dependencies.json")

const rewriteJson = (cwd, destPath, version, bundleName) => {
    const jsonDir = path.resolve(cwd, "bundle.json")
    const bundleJson = JSON.parse(fs.readFileSync(jsonDir).toString())
    if (bundleName.startsWith("third_party")) {
      bundleName = bundleName.replace("third_party_", "").replace("-", "_").toLowerCase()
    }
    if (bundleName) {
      bundleJson.name = `@ohos/${bundleName}`
    }
    if (bundleJson.envs) {
      bundleJson.envs = {}
    }
    if (bundleJson.dirs) {
      bundleJson.dirs = {}
    }
    if (bundleJson.scripts) {
      bundleJson.scripts = {}
    }

    bundleJson.segment = { destPath }
    bundleJson.version = version
    bundleJson.publishAs = "code-segment"

    let thridPart = bundleJson.component && 
      bundleJson.component.deps && 
      (bundleJson.component.deps.third_party || 
      bundleJson.component.deps.third_part)
      
    if (thridPart && thridPart.length) {
      bundleJson.dependencies = {}
      for (let index = 0; index < thridPart.length; index++) {
        const key = thridPart[index];
        if (!key) {
          continue
        }
        if (error_dependencies_json.hasOwnProperty(key) && !error_dependencies_json[key]) {
          continue
        }
        let realKey = ""
        if (error_dependencies_json[key]) {
          const { name, version: jsonVersion } =  error_dependencies_json[key]
          realKey = generateKey(name)
          bundleJson.dependencies[realKey] = `~${jsonVersion}`
        } else {
          realKey = key
          realKey = generateKey(realKey)
          bundleJson.dependencies[realKey] = `~3.1.1`
        }
      }
    }

    if (bundleName && dependencies_json[bundleName] && dependencies_json[bundleName].length) {
      if (!bundleJson.dependencies) {
        bundleJson.dependencies = {}
      }
      dependencies_json[bundleName].forEach(({ name, version: jsonVersion }) => {
        const realKey = generateKey(name)
        bundleJson.dependencies[realKey] = `~${jsonVersion}`
      })
    }

    if (bundleJson.keywords && bundleJson.keywords.length) {
      bundleJson.keywords = bundleJson.keywords.map(item => item.replace(/_/g, "-"))
    }

    if (bundleJson.tags && bundleJson.tags.length) {
      bundleJson.tags = bundleJson.tags.map(item => item.replace(/_/g, "-"))
    }

    if (!bundleJson.component.subsystem && !destPath.startsWith("third_party/")) {
      if (destPath.startsWith("device/")) {
        bundleJson.component.subsystem = "devices"
      } else if (destPath.startsWith("vendor/")) {
        bundleJson.component.subsystem = "vendor"
      } else if (destPath.startsWith("kernel/")) {
        bundleJson.component.subsystem = "kernel"
      } else if (destPath.startsWith("drivers/")) {
        bundleJson.component.subsystem = "hdf"
      } else if (destPath.startsWith("developtools/")) {
        bundleJson.component.subsystem = "developtools"
      } else if (destPath.startsWith("applications/")) {
        bundleJson.component.subsystem = "applications"
      } else {
        const subsystem = destPath.split("/")[1]
        bundleJson.component.subsystem = subsystem
      }
    }

    fs.writeFileSync(jsonDir, JSON.stringify(bundleJson, null, 4))
}

function generateKey(key) {
  let realkey = key.replace("third_party_", "").replace("-", "_").toLowerCase()
  if (!realkey.startsWith("@ohos")) {
    realkey = `@ohos/${realkey}`
  }
  return realkey
}

const [ cwd, destPath, version, name ] = process.argv.slice(2)
rewriteJson(cwd, destPath, version, name)