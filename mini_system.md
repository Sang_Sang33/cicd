### ohos_mini_system

## 简介
### 轻量系统设备(mini system)
面向MCU类处理器例如Arm Cortex-M、RISC-V 32为的设备，硬件资源极其有限，支持的设备最小内存为128KiB，可以提供多种轻量级网络协议，轻量级的图形框架，以及丰富的IOT总线读写部件等。可支撑的产品如智能家居领域的连接类模组、传感器设备、穿戴类设备等。

## 如何使用
本发行版预置了润和hispark_pegasus、小熊派、恒玄轻量级带屏设备、汇顶蓝牙连接设备四种形态的所有组件。你可以根据自己的需求挑选及配置适合自己的组件。

⚠️请在Linux环境下执行

1. 进入一个项目目录，在工作目录下执行如下命令

```shell
hpm init -t dist
hpm i @ohos/mini_system
```

说明：

```
hpm init -t dist ： 表示通过dist模板创建一个项目
hpm i xx 表示安装一个xx组件/发行版到你的项目，此过程会将所有依赖的组件（包括间接依赖）和工具下载安装到本机
```

2. 执行编译命令

```shell
hpm dist 
```
执行`hispark_pegasus`发行版的编译构建

```shell
hpm dist bearpi
```
执行`bearpi_hm_nano`发行版的编译构建

```shell
hpm dist bestechnic
```
执行`bestechnic`发行版的编译构建

```shell
hpm dist goodix
```
执行`gr5515`发行版的编译构建

说明：

```
hpm dist 表示执行发行版的编译构建，将开始制作镜像，运行结束后，文件将会输出到out目录下。
```